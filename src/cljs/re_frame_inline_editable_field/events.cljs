(ns re-frame-inline-editable-field.events
  (:require
   [re-frame.core :as re-frame]
   [re-frame-inline-editable-field.db :as db]))


(re-frame/reg-event-db
 ::initialize-db
 (fn [_ _]
   db/default-db))

(re-frame/reg-event-db
 ::set-active-panel
 (fn [db [_ active-panel]]
   (assoc db :active-panel active-panel)))

(re-frame/reg-event-db
  ::title
  (fn [db [_ id title]]
    (assoc-in db [:movies id :title] title)))

(re-frame/reg-event-db
  ::description
  (fn [db [_ id description]]
    (assoc-in db [:movies id :description] description)))
