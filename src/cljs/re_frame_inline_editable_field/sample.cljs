(ns re-frame-inline-editable-field.sample
  (:require
    [re-frame.core :as rf]
    [reagent.core :as reagent]
    [re-frame-inline-editable-field.events :as events]
    [re-frame-inline-editable-field.subs :as subs]))

(defn inline-editor [text on-change]
  (let [s  (reagent/atom {})]
    (fn [text on-change]
      [:span
       ;; (pr-str @s)
       (if (:editing? @s)
         [:form {:on-submit #(do
                               (.preventDefault %)
                               (swap! s dissoc :editing?)
                               (when on-change
                                 (on-change (:text @s))))}
          [:input {:type      :text :value (:text @s)
                   :on-change #(swap! s assoc
                                      :text (-> % .-target .-value))}]
          [:button "Save"]
          [:button {:on-click #(do
                                 (.preventDefault %)
                                 (swap! s dissoc :editing?))}
           "Cancel"]]
         [:span
          {:on-click #(swap! s assoc
                             :editing? true
                             :text text)}
          text [:sup " ✎"]])])))

(defn turorial-panel []
  [:div
   [:div (pr-str @(rf/subscribe [::subs/movies]))]
   (for [[movie-id movie] @(rf/subscribe [::subs/movies])]
     [:div {:key movie-id}
      [:h3 [inline-editor (:title movie)
            #(rf/dispatch [::events/title movie-id %])]]
      [:div [inline-editor (:description movie)
             #(rf/dispatch [::events/description movie-id %])]]])])

